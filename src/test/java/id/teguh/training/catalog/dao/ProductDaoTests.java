package id.teguh.training.catalog.dao;

import com.google.common.collect.Iterables;
import id.teguh.training.catalog.CatalogApplication;
import id.teguh.training.catalog.entity.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CatalogApplication.class)
@WebAppConfiguration
@Transactional
@Sql(scripts = {"/sql/delete_sample_product.sql", "/sql/insert_sample_product.sql"})
public class ProductDaoTests {
    @Autowired private ProductDao productDao;

    @Test
    public void testSaveProduct(){
        Product p = new Product();
        p.setCode("T-001");
        p.setName("Test Product 001");
        p.setPrice(new BigDecimal(2500000.01));
        p.setWeight(new BigDecimal(1000));

        Assert.assertNull(p.getId());
        productDao.save(p);
        Assert.assertNotNull(p.getId());
    }

    @Test
    public void testFindById(){
        Product p = productDao.findById(1L).orElse(null);
        Assert.assertNotNull(p);
        Assert.assertEquals("P-100", p.getCode());
        Assert.assertEquals("Product 100", p.getName());
        Assert.assertEquals(BigDecimal.valueOf(101000.01), p.getPrice());

        Assert.assertNull(productDao.findById(9999999999999L).orElse(null));
    }

    @Test
    public void testFindAll(){
        Iterable<Product> products = productDao.findAll();
        Assert.assertNotNull(products);
        Assert.assertTrue(!Iterables.isEmpty(products));
    }
}
