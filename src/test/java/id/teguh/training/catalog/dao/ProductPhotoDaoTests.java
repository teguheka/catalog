package id.teguh.training.catalog.dao;

import id.teguh.training.catalog.CatalogApplication;
import id.teguh.training.catalog.entity.Product;
import id.teguh.training.catalog.entity.ProductPhoto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CatalogApplication.class)
@WebAppConfiguration
@Transactional
@Sql(scripts = {"/sql/delete_sample_product_photo.sql", "/sql/insert_sample_product_photo.sql"})
public class ProductPhotoDaoTests {
    @Autowired private ProductPhotoDao productPhotoDao;
    @Autowired private ProductDao productDao;

    @Test
    public void testFindById() {
        Product product = productDao.findById(1L).orElse(null);
        Iterable<ProductPhoto> productPhotos = productPhotoDao.findByProduct(product);
        Assert.assertNotNull(productPhotos);
    }
}
