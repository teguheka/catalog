CREATE TABLE product (
	"id" SERIAL NOT NULL,
	"created_date" timestamp(6) DEFAULT now(),
	"updated_date" timestamp(6) DEFAULT now(),
	"version" int8 DEFAULT 1,
	"code" varchar(255) NOT NULL,
	"name" varchar(255) NOT NULL,
	"price" numeric(19,2) default 12.34,
	"weight" numeric(8,2) default 100000.00,
	PRIMARY KEY (id),
	UNIQUE (code)
);

-- INSERT INTO product (id, code, name) values (1, 'P-001', 'Product 1');
-- INSERT INTO product (id, code, name) values (2, 'P-002', 'Product 2');
-- INSERT INTO product (id, code, name) values (3, 'P-003', 'Product 3');
-- INSERT INTO product (id, code, name) values (4, 'P-004', 'Product 4');
-- INSERT INTO product (id, code, name) values (5, 'P-005', 'Product 5');
-- INSERT INTO product (id, code, name) values (6, 'P-006', 'Product 6');
-- INSERT INTO product (id, code, name) values (7, 'P-007', 'Product 7');
-- INSERT INTO product (id, code, name) values (8, 'P-008', 'Product 8');
-- INSERT INTO product (id, code, name) values (9, 'P-009', 'Product 9');
-- INSERT INTO product (id, code, name) values (10, 'P-010', 'Product 10');
-- INSERT INTO product (id, code, name) values (11, 'P-011', 'Product 11');
-- INSERT INTO product (id, code, name) values (12, 'P-012', 'Product 12');
-- INSERT INTO product (id, code, name) values (13, 'P-013', 'Product 13');
-- INSERT INTO product (id, code, name) values (14, 'P-014', 'Product 14');
-- INSERT INTO product (id, code, name) values (15, 'P-015', 'Product 15');

CREATE TABLE product_photo (
	"id" SERIAL NOT NULL,
	"created_date" timestamp(6) DEFAULT now(),
	"updated_date" timestamp(6) DEFAULT now(),
	"version" int8,
	"url" varchar(255) NOT NULL,
	"id_product" int8,
	PRIMARY KEY (id),
	UNIQUE (id_product, url)
);

-- insert into product_photo(id, id_product, url) VALUES (1, 1, '/6380f8c9-f022-4148-8de7-6dd0438a6aad/img001.jpg');
-- insert into product_photo(id, id_product, url) VALUES (2, 1, '/6380f8c9-f022-4148-8de7-6dd0438a6aad/img002.jpg');
-- insert into product_photo(id, id_product, url) VALUES (3, 2, '/6380f8c9-f022-4148-8de7-6dd0438a6aad/img003.jpg');
-- insert into product_photo(id, id_product, url) VALUES (4, 2, '/f747b8c1-dfbd-464f-b911-ae8131bfba7a/img001.jpg');
-- insert into product_photo(id, id_product, url) VALUES (5, 2, '/f747b8c1-dfbd-464f-b911-ae8131bfba7a/img002.jpg');
-- insert into product_photo(id, id_product, url) VALUES (6, 3, '/f747b8c1-dfbd-464f-b911-ae8131bfba7a/img003.jpg');
-- insert into product_photo(id, id_product, url) VALUES (7, 3, '/231ddc7b-a96c-4ce4-a234-4a841e6f29b1/img001.jpg');
-- insert into product_photo(id, id_product, url) VALUES (8, 3, '/231ddc7b-a96c-4ce4-a234-4a841e6f29b1/img002.jpg');
-- insert into product_photo(id, id_product, url) VALUES (9, 4, '/231ddc7b-a96c-4ce4-a234-4a841e6f29b1/img003.jpg');