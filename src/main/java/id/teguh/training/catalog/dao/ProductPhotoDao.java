package id.teguh.training.catalog.dao;

import id.teguh.training.catalog.entity.Product;
import id.teguh.training.catalog.entity.ProductPhoto;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductPhotoDao extends PagingAndSortingRepository<ProductPhoto, Long> {
    Iterable<ProductPhoto> findByProduct(Product product);
    ProductPhoto findByProductId(Long id);
}
