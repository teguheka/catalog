package id.teguh.training.catalog.dao;

import id.teguh.training.catalog.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, Long>{
    Page<Product> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
