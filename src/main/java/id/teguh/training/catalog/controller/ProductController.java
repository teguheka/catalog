package id.teguh.training.catalog.controller;

import id.teguh.training.catalog.dao.ProductDao;
import id.teguh.training.catalog.entity.Product;
import id.teguh.training.catalog.entity.ProductPhoto;
import id.teguh.training.catalog.exception.DataNotFoundException;
import id.teguh.training.catalog.service.ProductPhotoService;
import id.teguh.training.catalog.util.NullAwareBeanUtilsBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/products")
@Api(description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of Product.")
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired private ProductDao productDao;
    @Autowired private ProductPhotoService productPhotoService;

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("${productcontroller.findProducts}")
    public Page<Product> findProducts(Pageable pageable, @RequestParam(required = false) String name) {
        logger.info("get products with request: {}", pageable);
        if (StringUtils.isNotEmpty(name)) {
            return productDao.findByNameContainingIgnoreCase(name, pageable);
        } else {
            return productDao.findAll(pageable);
        }
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("${productcontroller.findById}")
    public Product findById(@PathVariable("id") Product product) {
        logger.info("get detail product {}", product);
        if (product == null) {
            throw new DataNotFoundException("No data with specified id");
        }
        return product;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("${productcontroller.saveProduct}")
    public Product saveProduct(@RequestBody @Valid Product product) {
        logger.info("save a new product {}", product);
        productDao.save(product);
        return product;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("${productcontroller.updateProduct}")
    public void updateProduct(@PathVariable("id") Product product, @RequestBody Product xProduct) throws InvocationTargetException, IllegalAccessException {
        logger.info("update product from {} to {}", product, xProduct);
        BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
        notNull.copyProperties(product, xProduct);
        productDao.save(product);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("${productcontroller.deleteProduct}")
    public void deleteProduct(@PathVariable Long id) {
        if (!productDao.existsById(id)) {
            throw new DataNotFoundException(String.format("No data with id: %s", id));
        }
        logger.info("delete product {}", id);
        productDao.deleteById(id);
    }

    @GetMapping("/{id}/photos")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("${productcontroller.findPhotoForProduct}")
    public Iterable<ProductPhoto> findPhotoForProduct(@PathVariable("id") Product product) {
        logger.info("get product photo {}", product.getId());
        return productPhotoService.findByProduct(product);
    }

    @PostMapping("/{id}/photos")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("${productcontroller.uploadPhotoForProduct}")
    public ProductPhoto uploadPhotoForProduct(@PathVariable Long id, @RequestParam(value = "file") MultipartFile file) {
        logger.info("save image to product {}", id);
        if (!productDao.existsById(id)) {
            throw new DataNotFoundException("No data with specified id " + id);
        }

        ProductPhoto newProductPhoto = new ProductPhoto();

        String url = productPhotoService.uploadImage(file);
        newProductPhoto.setUrl(url);

        Optional<Product> product = productDao.findById(id);
        newProductPhoto.setProduct(product.orElse(null));

        productPhotoService.save(newProductPhoto);
        return newProductPhoto;
    }

    @PatchMapping("/{id}/photos")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("${productcontroller.changeProductPhotoUrl}")
    public void updateProductPhotoUrl(@PathVariable Long id, @RequestParam String url) {
        if (!productDao.existsById(id)) {
            throw new DataNotFoundException("No data with product id " + id);
        }

        ProductPhoto xProductPhoto = productPhotoService.getByProductId(id);
        xProductPhoto.setUrl(url);
        productPhotoService.save(xProductPhoto);
    }
}
