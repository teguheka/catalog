package id.teguh.training.catalog.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_photo")
@ApiModel(description = "Class representing a product photo.")
public class ProductPhoto extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    @ApiModelProperty(notes = "${productPhoto.id}", example = "1", position = 0)
    protected Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_product", nullable = false)
    @ApiModelProperty(notes = "${productPhoto.product_id}", example = "1", required = true, position = 1)
    private Product product;

    @NotNull @NotEmpty
    @ApiModelProperty(notes = "${productPhoto.url}", example = "http://xxx.com", required = true, position = 2)
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

