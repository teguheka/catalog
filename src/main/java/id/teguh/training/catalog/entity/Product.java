package id.teguh.training.catalog.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@ApiModel(description = "Class representing a product.")
public class Product extends BaseIdEntity {
    @NotNull
    @NotEmpty
    @Column(unique = true)
    @ApiModelProperty(notes = "${product.code}", example = "P-001", required = true, position = 1)
    private String code;

    @NotNull
    @NotEmpty
    @ApiModelProperty(notes = "${product.name}", example = "Product 1", required = true, position = 2)
    private String name;

    @NotNull
    @Min(1)
    @ApiModelProperty(notes = "${product.weight}", example = "11", required = true, position = 3)
    private BigDecimal weight;

    @NotNull
    @Min(1000)
    @ApiModelProperty(notes = "${product.price}", example = "5000", required = true, position = 4)
    private BigDecimal price;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
