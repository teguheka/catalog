package id.teguh.training.catalog.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:custom.properties")
@ConfigurationProperties(prefix = "app")
public class CustomProperties {
    private String imageUrl;
    private String imageCredential;

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImageCredential() {
        return imageCredential;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setImageCredential(String imageCredential) {
        this.imageCredential = imageCredential;
    }
}
