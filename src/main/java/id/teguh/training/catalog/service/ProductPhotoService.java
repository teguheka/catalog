package id.teguh.training.catalog.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.teguh.training.catalog.controller.ProductController;
import id.teguh.training.catalog.dao.ProductPhotoDao;
import id.teguh.training.catalog.entity.Product;
import id.teguh.training.catalog.entity.ProductPhoto;
import id.teguh.training.catalog.util.CustomProperties;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProductPhotoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Autowired private ProductPhotoDao productPhotoDao;
    @Autowired private CustomProperties customProperties;



    public Iterable<ProductPhoto> findByProduct(Product product) {
        return productPhotoDao.findByProduct(product);
    }

    public ProductPhoto save(ProductPhoto productPhoto) {
        return productPhotoDao.save(productPhoto);
    }

    public ProductPhoto getByProductId(Long id) {
        return productPhotoDao.findByProductId(id);
    }

    public String uploadImage(MultipartFile file) {
        try {
            ByteArrayResource fileAsResource = new ByteArrayResource(file.getBytes()) {
                @Override
                public String getFilename() {
                    return file.getOriginalFilename();
                }
            };
            // header
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            String base64ClientCredential = new String(Base64.encodeBase64(customProperties.getImageCredential().getBytes()));
            headers.add("Authorization", "Basic " + base64ClientCredential);

            // request param
            LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
            map.add("file", fileAsResource);

            HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(map, headers);
            String url = customProperties.getImageUrl() + "/api/v1/images";

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
            ObjectMapper mapper = new ObjectMapper();
            Map responseBody = mapper.readValue(responseEntity.getBody(), HashMap.class);

            return (String) responseBody.get("url");
        } catch (Exception e) {
            LOGGER.error("Failed to upload image", e);
            throw new RuntimeException("JsonException, message: " + e.getMessage());
        }
    }

}
