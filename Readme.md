# Komponen
  1. Web = karena aplikasi web
  2. devtools = otomatis restart ketika compile
  3. actuator = url healtcheck
  4. jpa = orm
  5. flyaway = database migration tool

# Remote Source Code to Gitlab
  1. git init (init project)
  2. git remote add gitlab git@gitlab.com:training-teguh/catalog.git (remote url)
  3. git add . (add to git stash) (.) = semua file
  4. git commit -m "commit pertama"
  5. git push gitlab master

# Remote Source Code to Heroku
  1. open tab setting in heroku
  2. find value heroku git url example: https://git.heroku.com/catalog-tep.git
  3. git remote add heroku https://git.heroku.com/catalog-tep.git (add to remote)
  4. git push heroku master (push to heroku)

# Otomasi Deployment Heroku

1. create file .gitlab-ci.yml
2. create variable on gitlab click setting CI/CD
3. add variable at secret-variable
4. add script at pom.xml (untuk tau versi yg dideploy)
        
        <plugin>
            <groupId>pl.project13.maven</groupId>
            <artifactId>git-commit-id-plugin</artifactId>
            <configuration>
                <failOnNoGitDirectory>false</failOnNoGitDirectory>
            </configuration>
        </plugin>
5. type ```heroku buildpacks:clear``` (optional: jika terjadi error)


# How to Log Heroku App
```
show log heroku = heroku logs -t --app catalog-tep
```

---

Open deployed project at

```
https://catalog-tep.herokuapp.com/swagger-ui.html
```